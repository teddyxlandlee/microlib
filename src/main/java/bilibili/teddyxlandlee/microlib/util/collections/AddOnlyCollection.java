package bilibili.teddyxlandlee.microlib.util.collections;

import java.util.Collection;

// marker
public interface AddOnlyCollection<R> extends Collection<R> {
}
