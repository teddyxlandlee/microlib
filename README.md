# MicroLib

![modloader:fabric](https://img.shields.io/static/v1?label=modloader&message=Fabric&color=brown)
![license:MIT](https://img.shields.io/static/v1?label=License&message=MIT&color=blue)

A fork of [donywang922/YWlib](https://github.com/donywang922/YWlib),
adding more extensions to Minecraft.

Note: This mod is compatible with YWlib. Enjoy!